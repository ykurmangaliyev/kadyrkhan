﻿using System;

namespace Problem2
{
    /// <inheritdoc />
    /// <summary>
    /// Traffic guard mode (режим регулировщика). 
    /// Traffic light blinks with yellow color which indicates that traffic guard is on duty. 
    /// </summary>
    public class TrafficGuardTrafficLightControl : TimerBasedTrafficLightControl
    {
        public TrafficGuardTrafficLightControl() : base(TimeSpan.FromSeconds(0.33)) // 0.33 is too low, just for example
        {
        }

        protected override void SecondTick()
        {
            switch (State.Color)
            {
                case TrafficLightStateColor.Off:
                    State = new TrafficLightState(TrafficLightStateColor.Yellow);
                    break;

                case TrafficLightStateColor.Yellow:
                    State = new TrafficLightState(TrafficLightStateColor.Off);
                    break;

                default:
                    throw new NotSupportedException($"Current behaviour does not support state color {State.Color:G}");
            }
        }
    }
}