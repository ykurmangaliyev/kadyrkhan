﻿using System;

namespace Problem2
{
    public interface ITrafficLightControl
    {
        event EventHandler<TrafficLightState> OnStateChange;
    }
}