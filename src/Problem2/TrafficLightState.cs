﻿namespace Problem2
{
    public enum TrafficLightStateColor
    {
        Off,
        Red,
        Yellow,
        Green
    }

    /// <summary>
    /// Class is immutable
    /// </summary>
    public struct TrafficLightState
    {
        public TrafficLightState(TrafficLightStateColor color, string displayText = "--")
        {
            Color = color;
            DisplayText = displayText;
        }

        public TrafficLightStateColor Color { get; }

        public string DisplayText { get; }
    }
}