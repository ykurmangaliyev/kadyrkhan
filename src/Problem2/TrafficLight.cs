﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem2
{
    public class TrafficLight
    {
        private static int _traficLightsCounter = 0;

        private readonly string _id;

        private ITrafficLightControl _control;
        private TrafficLightState _state;

        public TrafficLight()
        {
            _id = (++_traficLightsCounter).ToString();
            Console.WriteLine($"TrafficLight object #{_id} has been created.");
            State = new TrafficLightState(TrafficLightStateColor.Off);
        }

        public ITrafficLightControl Control
        {
            get => _control;
            set
            {
                Disable();
                _control = value;
                Enable();
            }
        }

        public TrafficLightState State
        {
            get => _state;
            set
            {
                Console.WriteLine($"[{_id}] Setting state: text = {value.DisplayText}, color = {value.Color:G}");
                _state = value;
            }
        }

        public void Disable()
        {
            Console.WriteLine($"[{_id}] Command: Disable");

            if (_control != null)
            {
                Console.WriteLine($"[{_id}] Detaching from current control.");
                _control.OnStateChange -= OnChangeState;
            }

            State = new TrafficLightState(TrafficLightStateColor.Off);
        }

        public void Enable()
        {
            Console.WriteLine($"[{_id}] Command: Enable");

            if (_control != null)
            {
                Console.WriteLine($"[{_id}] Attaching to current control.");
                _control.OnStateChange += OnChangeState;
            }
        }

        public void OnChangeState(object sender, TrafficLightState state)
        {
            State = state;
        }
    }
}
