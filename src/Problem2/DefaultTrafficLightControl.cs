﻿using System;

namespace Problem2
{
    public class DefaultTrafficLightControl : TimerBasedTrafficLightControl
    {
        private readonly int _green;
        private readonly int _yellow;
        private readonly int _red;

        private int _currentTimerValue;

        private string CurrentTimeValueString => _currentTimerValue.ToString("D2");

        public DefaultTrafficLightControl(int green, int yellow, int red) : base(TimeSpan.FromSeconds(1))
        {
            // TODO: consider check arguments for limits (positive etc.)

            _green = green;
            _yellow = yellow;
            _red = red;

            _currentTimerValue = _yellow;
            State = new TrafficLightState(TrafficLightStateColor.Yellow);
        }

        protected override void SecondTick()
        {
            if (_currentTimerValue > 1)
            {
                _currentTimerValue--;
                State = new TrafficLightState(State.Color, CurrentTimeValueString);
                return;
            }

            switch (State.Color)
            {
                case TrafficLightStateColor.Red:
                    _currentTimerValue = _green;
                    State = new TrafficLightState(TrafficLightStateColor.Green, CurrentTimeValueString);
                    break;

                case TrafficLightStateColor.Yellow:
                    _currentTimerValue = _red;
                    State = new TrafficLightState(TrafficLightStateColor.Red, CurrentTimeValueString);
                    break;

                case TrafficLightStateColor.Green:
                    _currentTimerValue = _yellow;
                    State = new TrafficLightState(TrafficLightStateColor.Yellow, CurrentTimeValueString);
                    break;

                default:
                    throw new NotSupportedException($"Current behaviour does not support state color {State.Color:G}");
            }
        }
    }
}