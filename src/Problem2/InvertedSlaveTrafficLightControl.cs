﻿using System;

namespace Problem2
{
    public class InvertedSlaveTrafficLightControl : ITrafficLightControl, IDisposable
    {
        public event EventHandler<TrafficLightState> OnStateChange;

        private readonly ITrafficLightControl _masterControl;
        private bool _disposed;

        public InvertedSlaveTrafficLightControl(ITrafficLightControl masterControl)
        {
            // TODO: consider check arguments for null

            _masterControl = masterControl;
            _masterControl.OnStateChange += MasterControlOnOnStateChange;
        }

        private void MasterControlOnOnStateChange(object sender, TrafficLightState masterState)
        {
            TrafficLightState slaveState;

            switch (masterState.Color)
            {
                case TrafficLightStateColor.Red:

                    slaveState = new TrafficLightState(TrafficLightStateColor.Green, masterState.DisplayText);
                    break;

                case TrafficLightStateColor.Yellow:
                    slaveState = new TrafficLightState(TrafficLightStateColor.Red, masterState.DisplayText);
                    break;

                case TrafficLightStateColor.Green:
                    slaveState = new TrafficLightState(TrafficLightStateColor.Red, masterState.DisplayText);
                    break;

                default:
                    throw new NotSupportedException($"Current behaviour does not support state color {masterState.Color:G}");
            }

            OnStateChange?.Invoke(this, slaveState);
        }

        public void Dispose()
        {
            if (_disposed) return;

            _disposed = true;
            _masterControl.OnStateChange -= MasterControlOnOnStateChange;
        }
    }
}