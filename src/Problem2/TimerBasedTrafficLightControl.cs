﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Problem2
{
    public abstract class TimerBasedTrafficLightControl : ITrafficLightControl, IDisposable
    {
        private TrafficLightState _state;
        private bool _disposed;

        protected TrafficLightState State
        {
            get => _state;
            set
            {
                _state = value;
                OnStateChange?.Invoke(this, value);
            }
        }

        private Timer Timer { get; }

        public event EventHandler<TrafficLightState> OnStateChange;

        protected TimerBasedTrafficLightControl(TimeSpan interval)
        {
            Timer = new Timer((o) => SecondTick(), null, interval, interval);
        }

        protected abstract void SecondTick();

        public void Dispose()
        {
            if (_disposed) return;

            _disposed = true;
            Timer?.Dispose();
        }
    }
}