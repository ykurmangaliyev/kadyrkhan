﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Problem1;

namespace Problem1Console
{
    class Program
    {
        static void Main()
        {
            Console.WriteLine("Ready! Start by entering M and K, separated by space:");
            
            try
            {
                var stdIn = Console.OpenStandardInput();

                // Read input from console:
                var input = StreamProblem1InputReader.Read(stdIn); 

                // Solve the problem:
                var result = Problem1Solver.Solve(input);

                Console.WriteLine($"Average: {result.Average:F2}. Median: {result.Median:F2}. Press any key to EXIT...");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error has occured: {ex.Message}");
            }
            finally
            {
                Console.ReadLine();
                Environment.Exit(0);
            }
        }
    }
}
