﻿using System.Collections.Generic;
using System.Linq;

namespace Problem1
{
    public static class Problem1Solver
    {
        public static Problem1Solution Solve(bool[,] input)
        {
            var islands = new List<Island>();

            var cells = PrepareData(input);

            foreach (var cell in cells)
            {
                if (cell.IsEligibleForVisiting())
                {
                    var island = CreateNewIsland(cell, islands.Count + 1);
                    islands.Add(island);
                }
            }

            if (!islands.Any())
                return new Problem1Solution();
            
            islands = islands.OrderBy(x => x.Size).ToList();
            return new Problem1Solution
            {
                Average = islands.Average(isl => isl.Size),
                Median = islands.Count % 2 == 0
                    ? islands.Skip(islands.Count / 2 - 1).Take(2).Average(isl => isl.Size) 
                    : islands.Skip(islands.Count / 2).First().Size
            };
        }

        /// <summary>
        /// Breadth-first search
        /// </summary>
        /// <param name="cell">The cell where an island should originate from</param>
        /// <param name="id"></param>
        private static Island CreateNewIsland(Cell cell, int id)
        {
            var newIsland = new Island(id);
            var queue = new Queue<Cell>();

            newIsland.AttachCell(cell);
            queue.Enqueue(cell);

            while (queue.Any())
            {
                var nextCell = queue.Dequeue();

                foreach (var sibling in nextCell.Siblings)
                {
                    if (sibling.IsEligibleForVisiting())
                    {
                        newIsland.AttachCell(sibling);
                        queue.Enqueue(sibling);
                    }
                }
            }

            return newIsland;
        }

        /// <summary>
        /// Построение данных
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        private static Cell[,] PrepareData(bool[,] data)
        {
            int rows = data.GetLength(0),
                cols = data.GetLength(1);

            Cell[,] cells = new Cell[rows, cols];

            for (int r = 0; r < rows; r++)
            {
                for (int c = 0; c < cols; c++)
                {
                    var cell = new Cell(r, c, data[r, c]);

                    if (r > 0)
                    {
                        cell.MakeSiblingsWith(cells[r - 1, c]);
                    }

                    if (c > 0)
                    {
                        cell.MakeSiblingsWith(cells[r, c - 1]);
                    }

                    cells[r, c] = cell;
                }
            }

            return cells;
        }
    }
}
