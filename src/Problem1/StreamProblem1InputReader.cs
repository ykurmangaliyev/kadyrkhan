﻿using System;
using System.IO;
using System.Linq;

namespace Problem1
{
    /// <summary>
    /// Expected format: 
    /// M K
    /// ..... (K symbols of 0 or 1)
    /// .....
    /// .....
    /// (total M rows)
    /// 
    /// Example:
    /// 4 3
    /// 101
    /// 010
    /// 101
    /// 111 
    /// </summary>
    public class StreamProblem1InputReader
    {
        public static bool[,] Read(Stream stream)
        {
            using (var sr = new StreamReader(stream))
            {
                string firstLine = sr.ReadLine();

                if (string.IsNullOrWhiteSpace(firstLine))
                    throw new ArgumentException("Empty input.");

                string[] firstLineParts = firstLine.Split(' ');
                if (firstLineParts.Length != 2 || !int.TryParse(firstLineParts[0], out int rows) || !int.TryParse(firstLineParts[1], out int cols))
                    throw new ArgumentException("First line of input should be two integers (rows and cols, respectively) separated by space.");

                bool[,] terra = new bool[rows, cols];

                for (int r = 0; r < rows; r++)
                {
                    string rowLine = sr.ReadLine();
                    if (string.IsNullOrWhiteSpace(rowLine) || rowLine.Length != cols)
                        throw new ArgumentException($"Could not read row #{r + 1}, it should not be empty and must contain {cols} symbols");

                    for (int c = 0; c < cols; c++)
                    {
                        switch (rowLine[c])
                        {
                            case '1':
                                terra[r, c] = true;
                                break;

                            case '0':
                                terra[r, c] = false;
                                break;

                            default:
                                throw new ArgumentException($"Cell at row {r + 1}, cell {c + 1} contains unknown symbol: '{rowLine[c]}' while 0 or 1 expected");
                        }
                    }
                }

                return terra;
            }
        }   
    }
}