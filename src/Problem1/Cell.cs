﻿using System.Collections.Generic;

namespace Problem1
{
    public class Cell
    {
        public Cell(int row, int col, bool isTerra)
        {
            Row = row;
            Col = col;
            IsTerra = isTerra;
            Siblings = new HashSet<Cell>();
        }

        // Input data:
        public int Row { get; set; }

        public int Col { get; set; }

        public bool IsTerra { get; }

        // Intermediate values:

        public Island Island { get; set; }

        // Sibling references:
        public ISet<Cell> Siblings { get; set; }

        public void MakeSiblingsWith(Cell cell)
        {
            cell.Siblings.Add(this);
            Siblings.Add(cell);
        }

        public bool IsEligibleForVisiting()
        {
            return IsTerra && Island == null;
        }
    }
}