﻿using System;
using System.Collections.Generic;

namespace Problem1
{
    /// <summary>
    /// Компонента связности
    /// </summary>
    public class Island
    {
        private readonly ISet<Cell> _cells;

        public Island(int id)
        {
            Id = id;
            _cells = new HashSet<Cell>();
        }

        public int Id { get; }

        public void AttachCell(Cell cell)
        {
            if (cell.Island != null)
                throw new InvalidOperationException("Cell has already been attached to an island.");

            _cells.Add(cell);
            cell.Island = this;
        }

        public int Size => _cells.Count;
    }
}