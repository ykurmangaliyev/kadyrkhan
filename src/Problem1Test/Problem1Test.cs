﻿using System;
using System.IO;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Problem1;

namespace Problem1Test
{
    [TestClass]
    public class Problem1Test
    {
        private void AssertProblem1Solution(string stringInput, double average, double median)
        {
            using (var ms = new MemoryStream(Encoding.UTF8.GetBytes(stringInput)))
            {
                var parsed = StreamProblem1InputReader.Read(ms);

                var solution = Problem1Solver.Solve(parsed);

                // TODO: Consider floating number precision error-safe comparison (with epsilon)
                Assert.AreEqual(average, solution.Average);
                Assert.AreEqual(median, solution.Median);
            }
        }

        [TestMethod]
        public void Test1()
        {
            string input = @"3 3
101
010
101";

            AssertProblem1Solution(input, 1.0, 1.0);
        }

        [TestMethod]
        public void Test2()
        {
            string input = @"5 5
11101
11100
11101
11101
11100";

            AssertProblem1Solution(input, 6.0, 2.0);
        }

        [TestMethod]
        public void Test3()
        {
            string input = @"1 1
1";

            AssertProblem1Solution(input, 1.0, 1.0);
        }

        [TestMethod]
        public void Test4()
        {
            string input = @"1 1
0";

            AssertProblem1Solution(input, 0.0, 0.0);
        }

        [TestMethod]
        public void Test5()
        {
            string input = @"5 7
0000000
0000000
0000000
0000000
0000000";

            AssertProblem1Solution(input, 0.0, 0.0);
        }

        [TestMethod]
        public void Test6()
        {
            string input = @"5 7
0010001
0110001
0000000
0011001
0001000";

            AssertProblem1Solution(input, 2.25, 2.5);
        }

        [TestMethod]
        public void Test7()
        {
            string input = @"5 7
1111111
1100011
1101011
1100011
1111111";

            AssertProblem1Solution(input, 13.5, 13.5);
        }

        [TestMethod]
        public void Test8()
        {
            string input = @"5 7
1111101
1100000
1101011
1100011
1111111";

            AssertProblem1Solution(input, 8, 1.0);
        }
    }
}
