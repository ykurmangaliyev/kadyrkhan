﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Problem2;

namespace Problem2Console
{
    class Program
    {
        static void Main()
        {
            var trafficGuardControl = new TrafficGuardTrafficLightControl();
            var defaultControl = new DefaultTrafficLightControl(10, 3, 5);
            var invertedSlaveControl = new InvertedSlaveTrafficLightControl(defaultControl);

            var trafficLight = new TrafficLight();
            var trafficLight2 = new TrafficLight();

            Console.WriteLine($"Both traffic lights are switched off. Press Enter attach #1 to traffic guard control.");
            Console.ReadLine();

            trafficLight.Control = trafficGuardControl;
            Console.WriteLine($"#1 has been attached to traffic guard control. Press Enter to reattach #1 to default control.");
            Console.ReadLine();

            trafficLight.Control = defaultControl;
            Console.WriteLine($"#1 has been reattached to default control. Press Enter to attach #2 to the same control.");
            Console.ReadLine();

            trafficLight2.Control = defaultControl;
            Console.WriteLine($"#2 has been attached to the same control. Press Enter to detach #1 from control");
            Console.ReadLine();

            trafficLight.Control = null;
            Console.WriteLine($"#1 has been detached from default control, it is now disabled. Press Enter to attach #1 to inverted slave control.");
            Console.ReadLine();

            trafficLight.Control = invertedSlaveControl;
            Console.WriteLine($"#1 has been attached to inverted slave control. Press Enter to EXIT...");
            Console.ReadLine();
        }
    }
}
